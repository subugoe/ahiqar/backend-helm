# Helm Chart for Ahiqar Backend

## Local Usage

### TL;DR

```sh
NAME="ahiqar" && \
k3d cluster create ${NAME} --k3s-arg "--disable=traefik@server:0" -p "80:80@loadbalancer" && \
helm install \
  nginx-controller \
  oci://ghcr.io/nginxinc/charts/nginx-ingress \
  --version 1.1.3 \
  --namespace kube-system \
  --set controller.enableSnippets="true" && \
kubectl label ingressclasses.networking.k8s.io nginx ingressclass.kubernetes.io/is-default-class="true" && \
helm install ${NAME} ./backend/ --namespace ${NAME} --create-namespace --render-subchart-notes && \
kubectl config set-context --current --namespace="${NAME}"
```

### Local Registry with k3d

see: [k8s@sub Wiki](https://projects.academiccloud.de/projects/k8s-at-sub/wiki/set-up-a-cluster-with-a-registry)

### Local Cluster

We want to create a cluster alike our production environment, so we have to switch to nginx as ingress controller.

```sh
k3d cluster create --k3s-arg "--disable=traefik@server:0" -p "80:80@loadbalancer" ahiqar
# w/o ingress controller > curl localhost:80 -vvv → (52) Empty reply from server
helm install nginx-controller oci://ghcr.io/nginxinc/charts/nginx-ingress --version 1.1.3 --namespace kube-system
# with nginx > curl localhost:80 -vvv → 404
```

Troubleshooting: Might check the compatibility between kubernetes and nginx versions.

### Connect to the Remote Registry

The image is stored in a private registry. To access you have to add docker login credentials to your local cluster. Create a robot account at the respective project 'sub-fe' at [Harbor](https://harbor.gwdg.de).

```sh
kubectl create secret docker-registry harbor-sub --docker-server=harbor.gwdg.de --docker-username='' --docker-password=''
kubectl create secret docker-registry harbor-sub-fe --docker-server=harbor.gwdg.de --docker-username='' --docker-password=''
```

### Install

```sh
helm install ahiqar backend --render-subchart-notes
```

Carefully read the `NOTES` section in the output.

### Access the Services

To access the services you have to alter your `/etc/hosts` file to map the domain set in the `.Values.ingress.host` to match localhost.

```sh
cp /etc/hosts ./hosts.ahiqar.backup
printf "127.0.0.1 ahiqar.local api.ahiqar.local" | sudo tee -a /etc/hosts
```

#### Using a custom local port

!! NOT HEAVYLY TESTED YET !!

If you can not use port 80 for accessing a local cluster, e.g. you are
using a privileged container engine, you can also access the service by
running a local reverse proxy. You can either run the proxy service on the
priviledged port 80, or configure a rewrite like nginx `sub_filter` to
get make the system work entirely.

Assuming we started the local cluster listening on port `12123` we can
create an nginx proxy with the following configuration.

```sh
printf "
server {
    listen 80;
    server_name ahiqar.local;
    location / {
        proxy_pass http://ahiqar.local:12123/;
    }
}" > nginx.reverse-proxy.conf
docker run --name ahiqar-reverse-proxy --publish 80:80 nginx 
```

This will still require elevated rights for the container engine as a port below 1024 is used.

Some more settings has to be added to make it work on a port number < 1024:

```sh
printf "
server {
    listen 80;
    server_name ahiqar.local;
    location / {
        proxy_pass http://ahiqar.local:12123/;
        sub_filter_once off;
        sub_filter "ahiqar.local/" "ahiqar.local:12121/";
    }
}" > nginx.reverse-proxy.conf
docker run --name ahiqar-reverse-proxy --volume ./nginx.reverse-proxy.conf:/etc/nginx/conf.d/default.conf:ro --publish 12121:80 nginx

k3d cluster create -p "12123:80@loadbalancer" ahiqar
```

In this way the cluster can listen on port 12123 and an additional reverse proxy
is hosted outside of the cluster to rewrite URLs in accordance.

## Updating the Database and/or the Website

This Helm chart is structured as follows:

```bash
backend-helm
|__ backend
    |__ subcharts // contains the subcharts and their respective values.yaml
    |__ values.yaml // holds the values.yaml for local development
|__ instances // holds values.yaml for the different deployment environments
```

Since the subcharts should be executable on their own, they each contain a `values.yaml` that points to a certain image.
If you want to just execute the eXist-db or the website separately and update them, use these YAML file for updates.

The `backend` directory contains the `values.yaml` that is relevant for local execution.
If you want to update the databse and/or the website your local cluster, alter this `values` file.

Last but not least, if the database or the website in one of the remote k8s cluster should be changed, `instances/{ENV}.yaml` is the place to go.
