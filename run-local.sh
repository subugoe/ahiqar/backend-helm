#!/bin/bash
NAME="ahiqar"
k3d cluster delete ${NAME}
k3d cluster create ${NAME} --k3s-arg "--disable=traefik@server:0" -p "80:80@loadbalancer" && sleep 1

### to prevent kubectl or helm from being executed in a false context ###
CONTEXT=`kubectl config current-context`
if [ "${CONTEXT}" = "k3d-${NAME}" ]; then

  helm install \
    nginx-controller \
    oci://ghcr.io/nginxinc/charts/nginx-ingress \
    --version 1.1.3 \
    --namespace kube-system \
    --set controller.enableSnippets="true" && sleep 1
  kubectl label ingressclasses.networking.k8s.io nginx ingressclass.kubernetes.io/is-default-class="true" && sleep 1

  # import all images required
  IMAGES=$(helm template backend/ | \
    docker run -i --rm mikefarah/yq@sha256:eed5d07d2f6a6bd882be5519e98ca2cd3dc3819005820d80b83ae6f3d774e639 -o json | \
    jq --join-output '.spec.template.spec.containers[0].image | select( . != null )' | \
    sed -e "s/harbor/ harbor/g"
  )

  k3d image import --cluster ${NAME} ${IMAGES}

  helm install ${NAME} ./backend/ --namespace ${NAME} --create-namespace --render-subchart-notes && sleep 1
  kubectl config set-context --current --namespace="${NAME}"
else
        echo "Your kubectl context ist not k3d-${NAME}!, current context is: ${CONTEXT}"
        exit 1
fi
